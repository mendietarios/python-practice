print(10 > 9)
print(10 == 9)
print(10 < 9)

# *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
print("Sentencia IF: ")
a = 200
b = 33

if b > a:
  print("b is greater than a")
else:
  print("b is not greater than a")

# *-*-*-*-*-*-* Evaluando valores y variables *-*-*-*-*-*-*-*
print("Evaluate Values and variables")
print(bool("Hello"))
print(bool(15))

x = "Hello"
y = 15

print(bool(x))
print(bool(y))

# *-*-*-*-*-*-* La mayoria de los valores verdaderos *-*--*--*-*
print("Most values are true")
print(bool("abc"))
print(bool(123))
print(bool(["apple", "cherry", "banana"]))

# *-*-*-*-*-*-*-*- Algunos valores que son falsos *-*-*-*-*-*-*-*
print("Some values are False")
print(bool(False))
print(bool(None))
print(bool(0))
print(bool(""))
print(bool(()))
print(bool([]))
print(bool({}))

print("Clase: ")
class myclass():
  def __len__(self):
    return 0

myobj = myclass()
print(bool(myobj))

# *-*-*-*-**-*-*--*-*-**- Las funciones pueden retornar Booleanos *-*-*-*-*-*-**-
print("Functions can return a boolean")
def myFunction() :
  return True

print(myFunction())

# *-*- Other Example
def myFunction() :
  return False

if myFunction():
  print("YES!")
else:
  print("NO!")

# *-*-*-* Other Example

x = 200
print(isinstance(x, int))