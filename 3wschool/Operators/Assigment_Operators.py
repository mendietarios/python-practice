x = 5
print("Operador para asignar un valor a la variable = : " , x)

x += 3
print("Operador para realizar una suma a la variable += : ", x) 

x -= 2 
print("Operador para realizar una resta a la variable -= : ", x)

x *= 4
print("Operador para multiplicar la variable *= : " , x)

x /= 3
print("Operador de asignacion de division a la variable /= : " , x)

x %= 2
print("Operador de asignacion modulo de la variable %= : " , x)

x = 20
x //= 3
print("Operador de asignacion de division de enteros //= : ", x)

x **= 2
print("Operador de asignacion con exponentes **= : ", x)

#  Operacion de bit a bit AND
# realiza una operación AND bit a bit entre dos números
x = 20
x &= 5
print(x)

#  Operacion de bit a bit AND
# realiza una operación AND bit a bit entre dos números
x = 20
x |= 3
print(x)

#  Operacion de bit a bit XOR
# realiza una operación XOR bit a bit entre dos números
x = 20
x ^= 3
print(x)

# realiza una operación de desplazamiento a la derecha en los bits de un número y asigna el resultado a la variable original.
x = 20
x >>= 4
print(x)

# realiza una operación de desplazamiento a la izquierda en los bits de un número y asigna el resultado a la variable original.
x = 20
x >>= 4
print(x)

# Mis dudas
x = 20
print(x := 4)

