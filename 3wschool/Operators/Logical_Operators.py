x = 15
print("x: ", x )

print("Prueba de operadores logico AND : ")
print(x > 5 and  x < 10)
print(x < 5 and  x < 10)
print(x > 5 and  x > 10)

print("Prueba de operadores logico OR : ")
print(x > 5 or  x < 10)
print(x < 5 or  x < 10)
print(x > 5 or  x > 10)

print("Prueba de operadores logico NOT : ")
print(not(x > 5 and  x < 10))
print(not(x < 5 and  x < 10))
print(not(x > 5 and  x > 10))