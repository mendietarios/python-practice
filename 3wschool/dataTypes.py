import random

x = 1    # int
y = 2.8  # float
z = 1j   # complex

#convert from int to float:
a = float(x)

#convert from float to int:
b = int(y)

#convert from int to complex:
c = complex(x)

print(a)
print(b)
print(c)

print(type(a))
print(type(b))
print(type(c))

print(random.randrange(1,10))

print("It's alright")
print("He is called 'Johnny'")
print('He is called "Johnny"')


######################################################

a = """Lorem ipsum dolor sit amet,
consectetur adipiscing elit,
sed do eiusmod tempor incididunt
ut labore et dolore magna aliqua."""
print(a)

#####################################################

a = "Hello, World!"
print(a[1])


#######################################################

for x in "banana":
  print(x)


########################################################

a = "Hello, World!"
print(len(a))


###########################################################
txt = "The best things in life are free!"
print("bes" in txt)

##########################################################

txt = "The best things in life are free!"
print("besty" not in txt)

##########################################################

b = "Hello, World!"
print(b[2:5])

########################################################

b = "Hello, World!"
print(b[:5])

##############################################################

b = "Hello, World!"
print(b[2:])

#############################################################

b = "Hello, World!"
print(b[-5:-2])

###########################################################
#############################################################

a = "Hello, World!"
print(a.upper())

########################################################

a = "Hello, World!"
print(a.lower())

#############################################################

a = " Hello, World! "
print(a)
print(a.strip())

##############################################################

a = "Hello, World!"
print(a.replace("H", "J"))

##############################################################

a = "Hello, World!"
print(a.split("o"))

############################################################

a = "Hello"
b = "World"
c = a + " " + b
print(c)

############################################################

age = 36
txt = f"My name is John, I am {age}"
print(txt)

#########################################################

price = 59
txt = f"The price is {price} dollars"
print(txt)

#############################################################

price = 59
txt = f"The price is {price:.2f} dollars"
print(txt)

############################################################

txt = f"The price is {20 * 59} dollars"
print(txt)


########################################################
#String Methods

#capitalize()
txt = "hello, and welcome to my world."
x = txt.capitalize()
print (x)

#casefold()
txt = "Hello, And Welcome To My World!"
x = txt.casefold()
print(x)

#center()
txt = "banana"
x = txt.center(20)
print(x)

#count()
txt = "I love apples, apple are my favorite fruit"
x = txt.count("apple")
print(x)

#encode()
txt = "My name is Ståle"
x = txt.encode()
print(x)
print(txt.encode(encoding="ascii",errors="backslashreplace"))
print(txt.encode(encoding="ascii",errors="ignore"))
print(txt.encode(encoding="ascii",errors="namereplace"))
print(txt.encode(encoding="ascii",errors="replace"))
print(txt.encode(encoding="ascii",errors="xmlcharrefreplace"))

#endswith()
txt = "Hello, welcome to my world."
x = txt.endswith("world.")
print(x)

txt = "Hello, welcome to my world."
x = txt.endswith("my world.", 5, 11)
print(x)

#expandtabs()
txt = "H\te\tl\tl\to"
x =  txt.expandtabs(2)
print(x)
print(txt)
print(txt.expandtabs())
print(txt.expandtabs(2))
print(txt.expandtabs(4))
print(txt.expandtabs(10))

#find()
txt = "Hello, welcome to my world."
x = txt.find("to")
print(x)

x = txt.find("e", 5, 10)
print(x)

#format()
txt = "For only {price:.2f} dollars!"
print(txt.format(price = 49))

#index()
txt = "Hello, welcome to my world."
x = txt.index("e")
print(x)

#isalnum()
txt = "Company12"
x = txt.isalnum()
print(x)

#isalpha()
txt = "Company10"
x = txt.isalpha()
print(x)

#isascii()
txt = "Company123"
x = txt.isascii()
print("Prueba " + str(x))

#isdecimal()
##
txt = "prueba"
x = txt.isdecimal()
print(x)

txt = '12'
x = txt.isdecimal()
print(x)

#isdigit()
txt = "2"
x = txt.isdigit()
print(x)

#isidentifier()
txt = "Demo"
x = txt.isidentifier()
print(x)

a = "MyFolder"
b = "Demo002"
c = "2bring"
d = "my demo"

print("Pruebas de isidentifier()")
print(a.isidentifier())
print(b.isidentifier())
print(c.isidentifier())
print(d.isidentifier())

#islower()
txt = "hello world!"
x = txt.islower()
print(x)

##   isnumeric() y el isdecimal() funcionan de la misma manera a pesar de que tienen distinta descripcion
#isnumeric()
txt = "15"
x = txt.isnumeric()
print(x)

#isprintable()
txt = "Hello!\nAre you #1?"
x = txt.isprintable()
print(x)
txt = "Hello! Are you #1?"
x = txt.isprintable()
print(x)

#isspace()
txt = "   "
x = txt.isspace()
print(x)

txt = " Hola "
x = txt.isspace()
print(x)

#istitle()
txt = "Hello, And Welcome To My World!"
x = txt.istitle()
print(x)

txt = "Hello, and Welcome To My World!"
x = txt.istitle()
print(x)

#isupper()
print("Prueba de isupper(): ")
txt = "THIS IS NOW!"
x = txt.isupper()
print(x)

a = "Hello World!"
b = "hello 123"
c = "MY NAME IS PETER"

print(a.isupper())
print(b.isupper())
print(c.isupper())

#join()
print("Prueba con el join(): ")
myTuple = ("John", "Peter", "Vicky")
x = " ".join(myTuple)
print(x)

##Nota: Cuando se utiliza un diccionario como iterable, los valores devueltos son las claves, no los valores.
myDict = {"name": "John", "country": "Norway"}
mySeparator = "TEST"
x = mySeparator.join(myDict)
print(x)

#ljust()
print("*-*-*-*-*-*-*-*-*- Prueba de ljust()  *-*-*-*-*-*-*-*-*-*")
txt = "banana"
x = txt.ljust(20, "X")
print(x, "is my favorite fruit.")

#lower()
print("*-*-*-*-*-*-*-*-*- Prueba de lower()  *-*-*-*-*-*-*-*-*-*")
txt = "Hello my FRIENDS"
x = txt.lower()
print(x)

#lstrip()
print("*-*-*-*-*-*-*-*-*- Prueba de lstrip()  *-*-*-*-*-*-*-*-*-*")
txt = "     banana     "
x = txt.lstrip()
print("of all fruits", x, "is my favorite")

txt = ",,,,,ssaaww.....banana"
x = txt.lstrip(",.wsan")
print(x)

#maketrans()
print("*-*-*-*-*-*-*-*-*- Prueba de maketrans()  *-*-*-*-*-*-*-*-*-*")
txt = "Hello Sam!"
mytable = str.maketrans("S", "P")
print(txt.translate(mytable))

txt = "Hi Sam!"
x = "mSa"
y = "eJo"
mytable = str.maketrans(x, y)
print(txt.translate(mytable))

txt = "Good night Sam!"
x = "mSa"
y = "eJo"
z = "odnght"
mytable = str.maketrans(x, y, z)
print(mytable)
print(txt.translate(mytable))

#partition()
print("*-*-*-*-*-*-*-*-*- Prueba de partition()  *-*-*-*-*-*-*-*-*-*")
txt = "I could eat bananas all day"
x = txt.partition("bananas")
print(x)

txt = "I could eat bananas all day"
x = txt.partition("apples")
print(x)

#replace()
print("*-*-*-*-*-*-*-*-*- Prueba de replace()  *-*-*-*-*-*-*-*-*-*")
txt = "I like bananas"
x = txt.replace("bananas", "apples")
print(x)

txt = "one one was a race horse, two two was one too."
x = txt.replace("one", "three")
print(x)

txt = "one one was a race horse, two two was one too."
x = txt.replace("one", "three", 2)
print(x)

#rfind()
print("*-*-*-*-*-*-*-*-*- Prueba de rfind()  *-*-*-*-*-*-*-*-*-*")
txt = "Mi casa, su casa."
x = txt.rfind("casa")
print(x)

txt = "Hello, welcome to my world."
x = txt.rfind("e")
print(x)

txt = "Hello, welcome to my world."
print(txt.rfind("q"))
# print(txt.rindex("q"))

#rindex()
print("*-*-*-*-*-*-*-*-*- Prueba de rindex()  *-*-*-*-*-*-*-*-*-*")
txt = "Mi casa, su casa."
x = txt.rindex("casa")
print(x)

#rjust()
print("*-*-*-*-*-*-*-*-*- Prueba de rjust()  *-*-*-*-*-*-*-*-*-*")
txt = "banana"
x = txt.rjust(10)
print(x, "is my favorite fruit.")

txt = "banana"
x = txt.rjust(10, "O")
print(x)

#rpartition()
print("*-*-*-*-*-*-*-*-*- Prueba de rpartition()  *-*-*-*-*-*-*-*-*-*")
txt = "I could eat bananas all day, bananas are my favorite fruit"
x = txt.rpartition("bananas")
print(x)

#rsplit()
print("*-*-*-*-*-*-*-*-*- Prueba de rsplit()  *-*-*-*-*-*-*-*-*-*")
txt = "apple, banana, cherry"
x = txt.rsplit(", ")
print(x)

txt = "apple, banana, cherry"
x = txt.rsplit(", ", 1)
print(x)

#rstrip()
print("*-*-*-*-*-*-*-*-*- Prueba de rstrip()  *-*-*-*-*-*-*-*-*-*")
txt = "     banana     "
x = txt.rstrip()
print("of all fruits", x, "is my favorite")

txt = "banana,,,,,ssqqqww....."
x = txt.rstrip(",.qsw")
print(x)

#split()
print("*-*-*-*-*-*-*-*-*- Prueba de split()  *-*-*-*-*-*-*-*-*-*")
txt = "welcome to the jungle"
x = txt.split()
print(x)

txt = "hello, my name is Peter, I am 26 years old"
x = txt.split(", ")
print(x)

txt = "apple#banana#cherry#orange"
x = txt.split("#")
print(x)

txt = "apple#banana#cherry#orange"
x = txt.split("#", 1)
print(x)

#splitlines()
print("*-*-*-*-*-*-*-*-*- Prueba de splitlines()  *-*-*-*-*-*-*-*-*-*")
txt = "Thank you for the music\nWelcome to the jungle"
x = txt.splitlines()
print(x)

txt = "Thank you for the music\nWelcome to the jungle"
x = txt.splitlines(True)
print(x)

#startswith()
print("*-*-*-*-*-*-*-*-*- Prueba de startswith()  *-*-*-*-*-*-*-*-*-*")
txt = "My Hello, welcome to my world."
x = txt.startswith("Hello")
print(x)

txt = "Hello, welcome to my world."
x = txt.startswith("wel", 7, 20)
print(x)

#strip()
print("*-*-*-*-*-*-*-*-*- Prueba de strip()  *-*-*-*-*-*-*-*-*-*")
txt = "     banana     "
x = txt.strip()
print("of all fruits", x, "is my favorite")

txt = ",,,,,rrttgg.....banana....rrr"
x = txt.strip(",.grt")
print(x)

#swapcase()
print("*-*-*-*-*-*-*-*-*- Prueba de swapcase()  *-*-*-*-*-*-*-*-*-*")
txt = "Hello My Name Is PETER"
x = txt.swapcase()
print(x)

#title()
print("*-*-*-*-*-*-*-*-*- Prueba de title()  *-*-*-*-*-*-*-*-*-*")
txt = "Welcome to my world"
x = txt.title()
print(x)

txt = "hello b2b2b2 and 3g3g3g"
x = txt.title()
print(x)

#translate()
print("*-*-*-*-*-*-*-*-*- Prueba de translate()  *-*-*-*-*-*-*-*-*-*")
mydict = {83:  80}
txt = "Hello Sam!"
print(txt.translate(mydict))

txt = "Hi Sam!"
x = "mSa"
y = "eJo"
mytable = str.maketrans(x, y)
print(txt.translate(mytable))

txt = "Good night Sam!"
mydict = {109: 101, 83: 74, 97: 111, 111: None, 100: None, 110: None, 103: None, 104: None, 116: None}
print(txt.translate(mydict))

#upper()
print("*-*-*-*-*-*-*-*-*- Prueba de upper()  *-*-*-*-*-*-*-*-*-*")
txt = "Hello my friends"
x = txt.upper()
print(x)

#zfill()
print("*-*-*-*-*-*-*-*-*- Prueba de zfill()  *-*-*-*-*-*-*-*-*-*")
txt = "50"
x = txt.zfill(10)
print(x)

a = "hello"
b = "welcome to the jungle"
c = "10.000"

print(a.zfill(10))
print(b.zfill(10))
print(c.zfill(10))
