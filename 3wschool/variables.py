# x = 5
# y = "Jhon Doe"

# print(y , "  " , x)

# print(type(x))
# print(type(y))

##########################################
# x, y, z = "Orange", "Banana", "Cherry"
# print(x)
# print(y)
# print(z)

###########################
# x = y = z = "Orange"
# print(x)
# print(y)
# print(z)

#################
# fruits = ["apple", "banana", "cherry"]
# x, y, z = fruits
# print("this is fruits: " , fruits)
# print(x)
# print(y)
# print(z)

####################################################
# x = "Python "
# y = "is "
# z = "awesome"
# print(x + y + z)`

#####################################################
# x = 5
# y = "John"  
# print(x, y)

####################################################
# x = "awesome"

# def myfunc():
#     print("Python is " + x)

# myfunc()


#####################################################
# x = "awesome"

# def myfunc():
#   x = "fantastic"
#   print("Python is " + x)

# myfunc()

# print("Python is " + x)


########################################################
# def myfunc():
#   global x
#   x = "fantastic"

# myfunc()

# print("Python is " + x)


######################################################
x = "awesome"

def myfunc():
  global x
  x = "fantastic"

myfunc()

print("Python is " + x)